<?php

// application root
define('_ROOT', "");

// temporary files root
define('TMP_DIR', _ROOT.'_5TyIualPt/');

// composer root
define('VENDOR_DIR', _ROOT.'vendor/');

include_once(VENDOR_DIR.'autoload.php');

// instantiate app with the config folder route, relative to _ROOT
$app = new \Lib\smallApp( 'config_local/');

// render app
echo $app->run();