<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 5:49 PM
 */

namespace Lib;

class youtubeScrape
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $videoUrl;

    /**
     * @var string
     */
    protected $error;


    /**
     * @var string
     */
    protected $downloadedFile;

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    function download2mp3($url){
        $result = false;

        $this->videoUrl = $url;
        $this->id = $this->extractId($url);

        if( $this->id ){
            // download video
            if( $this->fetchVideoFile()){
                // extract audio and convert to MP3
                $mp3 = $this->extractMP3();
                if( $mp3 ){
                    $result = $mp3;
                }
            }
        } else {
            $this->error = "Error fetching ID from given URL";
        }

        return $result;
    }

    protected function fetchVideoFile(){
        $result = false;

        // get video info
        if( $videoInfo = $this->curlGetVideoInfo() ){

            // fetch the map
            $map = @$videoInfo[ smallApp::getConfigParam('mp3convert/urlMapKey') ];
            // parse the map
            parse_str( $map, $map );
            if( !isset( $map['url'])){
                $this->error = "Error fetching URL map";
            } else {
                // download the file and store the file name
                $fileRoot = TMP_DIR."video.".time().uniqid().".tmp";

                try{
                    _H::file_from_url( $map['url'],  $fileRoot );
                    $this->downloadedFile = $fileRoot;
                    $result = true;
                } catch (\Exception $e ){
                    $this->error = $e->getMessage();
                }
            }
        }

        return $result;
    }

    /**
     * Fetch video info for given youtube ID
     * @return bool|mixed
     */
    protected function curlGetVideoInfo(){

        $url = smallApp::getConfigParam('mp3convert/youtubeGetInfoUrl');
        $url = sprintf( $url, $this->id );

        $result =_H::urlContents($url);

        return $result;
    }

    protected function extractMP3(){
        $result = false;

        if( $this->downloadedFile ){
            // mp3 file name
            $filename = time()."_".uniqid().".mp3";

            // absolute fileroute for savin purposes
            $fileroute =
                smallApp::getConfigParam('absoluteRoutes/converted') .
                $filename;
            // store to file
            try{
                $video = $this->downloadedFile;

                $ffmpeg = smallApp::getConfigParam('mp3convert/ffmpegRoute');

                // execute ffmpeg
                // $cmd = "ffmpeg -i \"$video\" -ar 44100 -ab $ab -ac 2 \"$filename\"";
                $cmd = "$ffmpeg -i \"$video\" -ar 44100 -ac 2 \"$fileroute\" 2>&1";

                // $res = exec($cmd);
                exec($cmd, $res );

                // echo "<pre>";
                // var_dump( $res );


                // remove video file
                unlink( $video );
                clearstatcache();

                // if converted file exists,
                if( file_exists( $fileroute ) !== FALSE) {
                    // the site route to download file
                    $result =
                        smallApp::getConfigParam('siteHost').
                        smallApp::getConfigParam('AltoRouter/basePath')."/".
                        smallApp::getConfigParam('siteRoutes/converted').
                        $filename;
                } else {
                    $this->error = "Error converting audio to MP3";
                }

            } catch(\Exception $e ){
                $this->error = $e->getMessage();
            }
        }
        return $result;
    }

    function extractId($url){
        $result = null;

        $parse = parse_url($url);
        parse_str(@$parse['query'], $parse);
        $id = @$parse['v'];

        if( strlen($id) == smallApp::getConfigParam('mp3convert/youtubeIDLength') ){
            $result = $id;
        }

        unset($parse);
        return $result;
    }




}