<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 6:41 PM
 */

namespace Lib;


class smallApp
{

    /**
     * @var smallRouter
     */
    protected static $router;

    /**
     * @var string Route to config folder
     */
    protected static $config_route;

    /**
     * @var array contents of app.yml file
     */
    protected static $appConfig = array();

    /**
     * smallApp constructor.
     * @param string $config_route Route to config folder within site
     * @throws \Exception
     */
    function __construct( $config_route )
    {

        self::$appConfig    = \Spyc::YAMLLoad($config_route.'app.yml');
        if( !self::$appConfig){
            throw new \Exception('App config not found in route: '.$config_route );
        }

        $basePath = self::getConfigParam('AltoRouter/basePath', '');

        $map    = \Spyc::YAMLLoad($config_route.'map.yml');

        if( !self::$appConfig){
            throw new \Exception('Route map not found in route: '.$config_route );
        }

        self::$router       = new smallRouter( $map, $basePath );
    }

    /**
     * @param string $param  Path to config element within app.yml file, e.g. 'AltoRouter/basePath'
     * @return mixed
     */
    static function getConfigParam($param, $default = null ){
        $result = _H::arrayFetch( self::$appConfig, $param);
        if( is_null($result) ){
            $result = $default;
        }
        return $result;
    }

    function run(){
        // fetch route
        $route = self::$router->getAppRoute();
        if( @$route['controller'] ){
            $controllerName = self::getConfigParam('namespaces/controllers').@$route['controller'];
        } else {
            $controllerName = self::getConfigParam('namespaces/controllers').'defaultController';
        }

        // TODO: check if class exists

        /**
         * @var smallController $controller
         */
        $controller = new $controllerName();

        return $controller->callMethod( $route['method']);
    }
}