<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 7:18 PM
 */

namespace Lib;

class smallController
{

    function callMethod($method, $params = array() ){

        if( method_exists( $this, $method) ){
            return $this->$method( $params );
        }
        throw new \Exception("Method $method not found in controller");
    }

}