<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 8:51 PM
 */

namespace Lib;


class smallTpl {
    /**
     * Root within view_root
     * @var string
     */
    public $root;


    public $data = array();

    /**
     * Route to parent to root view folder, trailing slash required
     * @var string
     */
    public $view_root;

    public $allowed_extensions = array('.tpl', '.tpl.php', '.html', '.php' );


    /**
     * @param $filename
     * @param array $data
     * @param null $view_root
     * @return string
     */
    public static function render( $filename, $data = array(), $view_root = null  ){
        $tpl            = new self();
        $tpl->data      = $data;
        $tpl->view_root = $view_root ?: _ROOT.'Views/';
        $tpl->root      = $filename;

        $result = $tpl->html();
        unset( $tpl );
        return $result;

    }

    public function html() {
        $file = $this->view_root . $this->root;

        foreach( $this->allowed_extensions as $ext ){
            if ( file_exists( $file.$ext )) {
                extract($this->data);

                ob_start();

                include( $file.$ext );

                $content = ob_get_clean();

                return $content;
            }
        }

        trigger_error('Error: Could not load template ' . $file . '!');
        exit();

    }
}