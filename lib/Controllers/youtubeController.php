<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 7:23 PM
 */

namespace Lib\Controllers;


use Lib\smallController;
use Lib\smallTpl;
use Lib\youtubeScrape;

class youtubeController extends smallController
{
    function index(){


        $inputUrl = @$_GET['url'];
        $id = null;
        $url = null;

        if( $inputUrl ){
            /*
            $yt = new \yt_downloader($inputUrl);
            $id = $yt->get_video_id();
            try{

                $yt->download_audio();

                $audio = $yt->get_audio();
                $path_dl = $yt->get_downloads_dir();

                clearstatcache();
                if($audio !== FALSE && file_exists($path_dl . $audio) !== FALSE)
                {

                    $url = $path_dl . $audio;
                }
            } catch( \Exception $e ){
                $error = $e->getMessage();
            }
            */

            $you2 = new youtubeScrape();
            try{
                if( $url = $you2->download2mp3($inputUrl) ){
                    $id = $you2->getId();
                } else {
                    $error = $you2->getError();
                }
            } catch( \Exception $e ){
                $error = $e->getMessage();
            }
        }


        $data = compact('id','url');
        if( @$error ){
            $data['error'] = $error;
        }

        return  smallTpl::render('json', $data );
    }
}