<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 7:20 PM
 */

namespace Lib\Controllers;

use Lib\smallController;
use Lib\smallTpl;

class defaultController extends smallController
{

    function home(){
        return smallTpl::render('home');
    }

}