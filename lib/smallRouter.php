<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 6:42 PM
 *
 * This is to extend the functionality of AltoRouter
 */

namespace Lib;


class smallRouter extends \AltoRouter
{
    function getAppRoute()
    {

        // default empty result
        $result = array('controller'=>'', 'method'=>'', 'target'=>'' );

        // match route
        $route = $this->match();

        // split it into an array
        if( is_array($route ) ){
            $chunks                 = explode(':', $route['target'] );
            $route['controller']    = @$chunks[0] ?: null;
            $route['method']        = @$chunks[1] ?: 'index';
            $result = $route;
        }

        return $result;
    }
}