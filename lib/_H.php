<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11/9/2017
 * Time: 6:51 PM
 *
 * General helper class
 */

namespace Lib;


class _H
{

    /**
     * @var string
     */
    static $error;

    /**
     * Fetches a nested element from array by string path to it.
     * @param array $array - array to browse
     * @param string $path string , special symbol separated path. E.g. 'money/cash/daily/value' The symbol is set with $separator param, default '/'
     * @param string $childrenElement - the item that holds chidren elements array, in case each item has other properties than child elements
     * @param string $childrenElement - if passed, an element with this name is used to search within for children elements
     *                                  if not passed - child is looked for within the array itself
     * @param string $separator
     * @return mixed
     */
    public static function arrayFetch( $array, $path, $childrenElement = NULL, $separator='/' ){

        // trim first and last separators (eg for url sections)
        $path = trim( $path, $separator );

        // explode path
        $run = explode( $separator, $path );

        // get the first item from path
        $itemName = array_shift($run);

        // get the item
        $next = isset( $array[$itemName] ) ? $array[$itemName] : null;

        // if no children element is passed, look within the array itself
        if( is_null($childrenElement) ){
            $res = $next;
        } else {
            // if children element is passed, look within the children element
            $res = isset( $next[$childrenElement] ) ? $next[$childrenElement] : null;
        }

        // if not found, return null
        if( is_null( $res ) )
            return null;

        // if it was the last item, return it
        if( empty($run) ){
            return $res;
        } else {
            // otherwise - recursion
            return self::arrayFetch( $res, implode( $separator, $run ), $childrenElement, $separator );
        }

    }

    static function urlContents($url ){
        $result = null;

        try{
            $data = @file_get_contents( $url );
            parse_str($data, $result);
        } catch( \Exception $e ){
            self::$error = $e->getMessage();
        }

        return $result;
    }



    static function file_from_url($url, $tofile){
        file_put_contents( $tofile, file_get_contents($url));
    }
    /**
     * From https://stackoverflow.com/a/4000569/3770223
     * Copy remote file over HTTP one small chunk at a time.
     *
     * @param string $infile The full URL to the remote file
     * @param string $outfile The path where to save the file
     * @return bool|int
     */
    static function copyfile_chunked($infile, $outfile) {
        $chunksize = 10 * (1024 * 1024); // 10 Megs

        /**
         * parse_url breaks a part a URL into it's parts, i.e. host, path,
         * query string, etc.
         */
        $parts = parse_url($infile);
        $i_handle = fsockopen($parts['host'], 80, $errstr, $errcode, 5);
        $o_handle = fopen($outfile, 'wb');

        if ($i_handle == false || $o_handle == false) {
            return false;
        }

        if (!empty($parts['query'])) {
            $parts['path'] .= '?' . $parts['query'];
        }

        /**
         * Send the request to the server for the file
         */
        $request = "GET {$parts['path']} HTTP/1.1\r\n";
        $request .= "Host: {$parts['host']}\r\n";
        $request .= "User-Agent: Mozilla/5.0\r\n";
        $request .= "Keep-Alive: 115\r\n";
        $request .= "Connection: keep-alive\r\n\r\n";
        fwrite($i_handle, $request);

        /**
         * Now read the headers from the remote server. We'll need
         * to get the content length.
         */
        $headers = array();
        while(!feof($i_handle)) {
            $line = fgets($i_handle);
            if ($line == "\r\n") break;
            $headers[] = $line;
        }
        /**
         * Look for the Content-Length header, and get the size
         * of the remote file.
         */
        $length = 0;
        foreach($headers as $header) {
            if (stripos($header, 'Content-Length:') === 0) {
                $length = (int)str_replace('Content-Length: ', '', $header);
                break;
            }
        }

        /**
         * Start reading in the remote file, and writing it to the
         * local file one chunk at a time.
         */
        $cnt = 0;
        while(!feof($i_handle)) {
            $buf = '';
            $buf = fread($i_handle, $chunksize);
            $bytes = fwrite($o_handle, $buf);
            if ($bytes == false) {
                return false;
            }
            $cnt += $bytes;

            /**
             * We're done reading when we've reached the conent length
             */
            if ($cnt >= $length) break;
        }

        fclose($i_handle);
        fclose($o_handle);
        return $cnt;
    }

}